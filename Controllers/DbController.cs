using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API._Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers {
  [ApiController]
  [Route("api/[controller]")]
  public class DbController: ControllerBase {
    public DbController() {
      // here I would Dep. Inject
    }

    [HttpPost("upload")]
    public async Task < IActionResult > upload(List<Person> PersonData) {
      Console.WriteLine("We have hit the API");
      // DO THINGS WITH THE PERSONDATA
      foreach(Person x in PersonData) {
        Console.Write(x.PersonID + "\n");
      }
      // Return a 200 
      return Ok();
    }
  }
}